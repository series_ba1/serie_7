import java.sql.SQLOutput;
import java.util.Scanner;

public class Geometrie {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        Triangle triangle = new Triangle();
        for (int i = 1; i <= 3; ++i) {
            System.out.println("Construction d'un nouveau point :");
            System.out.print("    Veuillez entrez x : ");
            double val = scanner.nextDouble();
            switch (i) {
                case 1 -> triangle.point1.setX(val);
                case 2 -> triangle.point2.setX(val);
                case 3 -> triangle.point3.setX(val);
            }
            System.out.print("    Veuillez entrez y : ");
            val = scanner.nextDouble();
            switch (i) {
                case 1 -> triangle.point1.setY(val);
                case 2 -> triangle.point2.setY(val);
                case 3 -> triangle.point3.setY(val);
            }
        }
        System.out.println("Périmètre :" + triangle.perimetre());
        if(triangle.isIsocele())
            System.out.println("Le triangle est isocèle");
    }
}
class Triangle {
    Point point1 = new Point();
    Point point2 = new Point();
    Point point3 = new Point();
    private double calculerLongueur(Point point1, Point point2) {
        return Math.sqrt(Math.pow(point1.getX() - point2.getX(), 2) + Math.pow(point1.getY() - point2.getY(), 2));
    }
    public double perimetre() {
        return calculerLongueur(this.point1, this.point2) + calculerLongueur(this.point1, this.point3) + calculerLongueur(this.point2, this.point3);
    }
    public boolean isIsocele() {
        boolean isIsocele = calculerLongueur(this.point1, this.point2) ==  calculerLongueur(this.point1, this.point3);
        isIsocele |= calculerLongueur(this.point1, this.point2) ==  calculerLongueur(this.point2, this.point3);
        isIsocele |= calculerLongueur(this.point1, this.point3) ==  calculerLongueur(this.point2, this.point3);
        return isIsocele;
    }
}
class Point {
    double x;
    double y;

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}