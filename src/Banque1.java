class Banque1 {

	public static void main(String[] args) {
		nouveauClient client1 = new nouveauClient("Pedro", "Genève", true, 1000.0, 2000.0);
		nouveauClient client2 = new nouveauClient("Alexandra", "Lausanne", false, 3000.0, 4000.0);

		// Afficher les données du premier client:
		afficherClient(client1);
		// Afficher les données du deuxième client:
		afficherClient(client2);
		client1.bouclerCompte();
		client2.bouclerCompte();
		// Afficher les données du premier client:
		afficherClient(client1);
		// Afficher les données du deuxième client:
		afficherClient(client2);
	}

	static void afficherClient(nouveauClient client) {
		// Cette méthode affiche les données du client
		System.out.println("Client" + (client.isMasculin() ? " " : "e ") + client.getNom() + " de " + client.getVille());
		System.out.println("   Compte prive:     " + client.getSoldePrive() + " francs");
		System.out.println("   Compte d'epargne: " + client.getSoldeEpargne() + " francs");
	}

}
class nouveauClient {
	private String nom;
	private String ville;
	private double soldePrive;
	private double soldeEpargne;
	private double tauxPrive = 0.01;
	private double tauxEpargne = 0.02;
	private boolean masculin;

	public nouveauClient(String nom, String ville, boolean masculin, double soldePrive, double soldeEpargne) {
		this.nom = nom;
		this.ville = ville;
		this.masculin = masculin;
		this.soldePrive = soldePrive;
		this.soldeEpargne = soldeEpargne;
	}
	public void bouclerCompte() {
		// Cette méthode ajoute les intérêts au solde
		soldePrive = soldePrive + tauxPrive * soldePrive;
		soldeEpargne = soldeEpargne+ tauxEpargne * soldeEpargne;
	}

	public double getSoldeEpargne() {
		return soldeEpargne;
	}

	public double getSoldePrive() {
		return soldePrive;
	}

	public String getNom() {
		return nom;
	}

	public String getVille() {
		return ville;
	}

	public boolean isMasculin() {
		return masculin;
	}
}